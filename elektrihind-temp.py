# -*- coding: utf-8 -*-
# Margus Sumla
# 2018

import time
import json
import smtplib
import requests
import os, sys, getpass
import RPi.GPIO as GPIO
import Adafruit_MCP9808.MCP9808 as MCP9808

def clear_():
    try:
        os.system('clear')
    except:
        os.system('cls')

def get_interval_():
    try:
        interval_ = float(input('Interval to check grades after (press enter for default (1800 seconds)): '))
    except:
        interval_ = 1800.0

    return interval_

def mail_auth():
    dec = ''

    while dec != 'yes' and dec != 'no':
        dec = input('Do you wish to setup a g-mail account (no for default account)? [ yes / no ]: ').lower()

    if dec == 'yes':
        print('Make sure of "Allow less secure apps: ON" in gmail settings.')
        return [input('Gmail username: '), getpass.getpass('Gmail password: '), input('Send notifications to email: ')]
    else:
        return ['ambaaigar', 'r4g144bm4', input('Send notifications to email: ')]

def send_mail(auth, msg):
    mail_user = auth[0]
    mail_pass = auth[1]
    mail_send_to = auth[2]

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()

    try:
        server.login(mail_user, mail_pass)
    except:
        server.login('ambaaigar', 'r4g144bm4')
        mail_user = 'ambaaigar'

    server.sendmail(mail_user, mail_send_to, msg)
    server.quit()
    print('Email sent to ' + auth[2])

def get_prices():
    prices = []
    s = requests.Session()

    r = s.get('https://elektrihind.ee/api/latest.php?limit=5')
    r = r.text
    prices = json.loads(r)

    s.close()

    return prices

def c_to_f(c):
    return c * 9.0 / 5.0 + 32.0

def get_temperature():
    sensor = MCP9808.MCP9808()
    sensor.begin()
    return sensor.readTempC()

def switch_pin(pin_no, pin_mode):
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(pin_no, GPIO.OUT)

    if pin_mode:
        GPIO.output(pin_no, GPIO.HIGH)
        print('Pin ' + str(pin_no) + ' on')
    else:
        GPIO.output(pin_no, GPIO.LOW)
        print('Pin ' + str(pin_no) + ' off')

clear_()

while True:
    temperature = get_temperature()
    print('Temperatuur: {0:0.3F}*C / {1:0.3F}*F'.format(temperature, c_to_f(temperature)))

    if temperature > 25.6:
        prices = get_prices()

        for price in prices:
            if price['provider_name'] == 'Eesti Energia AS':
                if float(price['kwh_avg']) > 5.5:
                    switch_pin(17, 0)
                else:
                    switch_pin(17, 1)

                print(price['display_name'] + ' | ' +
                        price['kwh_avg'] + ' senti / kWh | ' +
                        price['provider_name'] + ' | ' +
                        price['residence'] + '\n')

                break

    else:
        switch_pin(17, 1)

    time.sleep(4)
