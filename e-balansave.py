# -*- coding: utf-8 -*-
# Margus Sumla
# 2018

import os
import time
import json
import requests
from dialog import Dialog
import RPi.GPIO as GPIO
import Adafruit_MCP9808.MCP9808 as MCP9808

# käsureaakna tühjendamine
def clear_screen():
    try:
        os.system('clear')
    except:
        os.system('cls')

# internetiühenduse kontrollimine
def test_connection():
    try:
        _ = requests.get('https://www.neti.ee')
        return True
    except requests.ConnectionError:
        return False

# tagastatavate sõnede tõlkimine inglise keelest eesti keelde
def translate(string):
    if string == 'house':
        return 'maja'
    elif string == 'apartment':
        return 'korter'

# konfiguratsioonifaili olemasolu kontrollimine
def config_exists():
    if os.path.isfile('./config.cfg'):
        return True
    else:
        return False

# konfiguratsioonifaili kirjutamine
def config_write(array):
    file = open('./config.cfg', 'w')

    for element in array:
        file.write(str(element) + '  ')

    file.close

# konfiguratsioonifaili lugemine
def config_read():
    file = open('./config.cfg', 'r')
    settings = file.read()
    file.close()

    settings = settings.split('  ')
    settings = list(filter(None, settings))

    for i in range(len(settings)):
        settings[i] = settings[i].lstrip()

    return settings

# börsipakettide pärimine ja töötlemine
def get_prices():
    counter = 1
    prices_list = []
    prices_tuples = []
    providers_list = []

    session = requests.Session()
    response = session.get('https://elektrihind.ee/api/latest.php?limit=50')
    response = response.text
    prices = json.loads(response)
    session.close()

    # pikima elektrimüüja nime leidmise massiivi loomine
    for price in prices:
        providers_list.append(price['provider_name'])

    provider_longest = '%-' + str(len(max(providers_list)) + 2) + 's'

    # päringust saadud börsipakettide nimetustest massiivi loomine
    for price in prices:
        prices_list.append(provider_longest % (price['provider_name']) +\
                           '%-8s' % (translate(price['residence'])) +\
                           price['display_name'])

    # korduvate variantide välja filtreerimine
    prices_list = list(set(prices_list))

    # pikima börsipaketi sõne täisarvulise muutuja loomine
    price_longest = len(max(prices_list))

    # kasutajaliidese jaoks väärtustepaaride loomine
    for price in prices_list:
        prices_tuples.append(('%d' % (counter), '%s' % (price)))
        counter += 1

    return [prices_tuples, price_longest, prices]

# börsipakettide töötlemine
def parse_prices(prices_list, provider_name, residence, display_name):
    for price in prices_list:
        if price['provider_name'] == provider_name and\
           translate(price['residence']) == residence and\
           price['display_name'] == display_name:
            return [price['provider_name'], translate(price['residence']),\
                    price['display_name'], float(price['kwh_avg'])]
            break

# temperatuuri mõõtmine
def get_temperature():
    sensor = MCP9808.MCP9808()
    sensor.begin()
    return sensor.readTempC()

# klemmi pingestamine
def switch_pin(pin_no, pin_mode):
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(pin_no, GPIO.OUT)

    if pin_mode:
        GPIO.output(pin_no, GPIO.HIGH)
        return 'Sees'
    else:
        GPIO.output(pin_no, GPIO.LOW)
        return 'Väljas'

# sisendandmete päring, võrdlus ja tagastamine ning klemmi pingestamine
def main(config_settings):
    prices = get_prices()
    temperature = get_temperature()
    pin_switched = None
    parsed_prices = parse_prices(prices[2], config_settings[0],\
                                 config_settings[1], config_settings[2])

    try:
        # kriitilise hinnapiiri kontroll
        if parsed_prices[3] > float(config_settings[3]):
            # elektriseadme sisselülitamise signaal
            pin_switched = switch_pin(17, 0)
        else:
            # ümberlülituse temperatuurilävendi kontroll
            if temperature > float(config_settings[5]):
                # ümberlülituse hinnalävendi kontroll
                if float(parsed_prices[3]) > float(config_settings[4]):
                    # elektriseadme väljalülitamise signaal
                    pin_switched = switch_pin(17, 0)
                else:
                    # elektriseadme sisselülitamise signaal
                    pin_switched = switch_pin(17, 1)
            else:
                # elektriseadme väljalülitamise signaal
                pin_switched = switch_pin(17, 1)

        # muutujate tagastamine, kui leiti hind
        return temperature, parsed_prices[3], pin_switched

    except:
        # muutujate tagastamine, kui ei leitud hinda
        return temperature, '-', 'Sees'

# internetiühenduse kontrollimise akna kuvamine
def tui_connection_check(d):
    code = d.pause('Internetiühendus puudub\
                   \n\nKontrollige ühendust ja valige "Uuesti"\
                   \nProgrammi peatamiseks valige "Välju"',\
            height=12, width=43,\
            seconds=10,\
            ok_label='Uuesti', cancel_label='Välju')

    if code == d.OK:
        pass
    else:
        exit()

# seadistuse tüübi valimise akna kuvamine
def tui_settings(d):
    if not config_exists():
        return '2'
    else:
        code, tag = d.menu('Palun valige automaatne või käsitsi seadistus',\
                                 choices=[('1', 'Soovin kasutada eelmise korra seadistust'),\
                                          ('2', 'Soovin sisestada uue seadistuse')],\
                                           width=50, menu_height=2, height=9,\
                                           ok_label='Alusta', cancel_label='Välju')

        if code == d.OK:
            return tag
        else:
            exit()

# börsipaketi valimise akna kuvamine
def tui_select_prices(d):
    choices_list = get_prices()

    code, tag = d.menu('Palun valige endale sobiv börsipakett',\
                       choices=choices_list[0],\
                       width=choices_list[1] + 20,\
                       menu_height=len(choices_list[0]),\
                       height=len(choices_list[0]) + 7,\
                       ok_label='Edasi', cancel_label='Välju')

    if code == d.OK:
        return choices_list[0][int(tag) - 1][1]
    else:
        exit()

# kriitilise hinnapiiri valimise akna kuvamine
def tui_select_pricelimit_critical(d):
    while True:
        code, answer = d.inputbox("Palun valige endale sobiv kriiti-\nline hinnapiir [€ senti / kWh]",\
                                  init='10.0', height=9, width=42,\
                                  ok_label='Edasi', cancel_label='Välju')

        if code == d.OK:
            try:
                return float(answer)
                break
            except:
                continue
        else:
            exit()

# hinnalävendi valimise akna kuvamine
def tui_select_pricelimit(d):
    while True:
        code, answer = d.inputbox("Palun valige endale sobiv ümber-\nlülituse hinnalävend [€ senti / kWh]",\
                                  init='5.5', height=9, width=42,\
                                  ok_label='Edasi', cancel_label='Välju')

        if code == d.OK:
            try:
                return float(answer)
                break
            except:
                continue
        else:
            exit()

# temperatuurilävendi valimise akna kuvamine
def tui_select_temperaturelimit(d):
    while True:
        code, answer = d.inputbox("Palun valige endale sobiv ümber-\nlülituse temperatuurilävend [kraadi C]",\
                                  init='21.0', height=9, width=42,\
                                  ok_label='Edasi', cancel_label='Välju')

        if code == d.OK:
            try:
                return float(answer)
                break
            except:
                continue
        else:
            exit()

# kontrollperioodi viite valimise akna kuvamine
def tui_select_checkintervallimit(d):
    while True:
        code, answer = d.inputbox("Palun valige endale sobiv\nkontrollperioodi viide [sekundit]",\
                                  init='10', height=9, width=42,\
                                  ok_label='Alusta', cancel_label='Välju')

        if code == d.OK:
            try:
                return int(answer)
                break
            except:
                continue
        else:
            exit()

# 
def tui_select_parameters(d):
    code, tag, item = d.inputmenu('Palun valige endale sobivad väärtused',\
                          choices=[('Kriitiline hinnapiir [€ senti / kWh]',),\
                                   ('Ümberlülituse hinnalävend [€ senti / kWh]', '5.5'),\
                                   ('Ümberlülituse temperatuurilävend [kraadi C]', '22.0'),\
                                   ('Kontrollperioodi viide [sekundit]', '10.0')],\
                                    width=51, menu_height=12, height=19,\
                                    ok_label='Edasi', cancel_label='Välju',\
                                    extra_label='Muuda')

# jooksvate andmete akna kuvamine
def tui_display_progress(d, config_settings, temperature, current_price, pin_switched):
    pricename = '%s, %s, %s' % (config_settings[0], config_settings[1], config_settings[2])

    code = d.pause('Valitud pakett: %s\
                   \n\nTemperatuur: %.1f* C\
                   \nHind: %s senti / kWh\
                   \nElektriseadme olek: %s' %\
                   (pricename, temperature, current_price, pin_switched),\
                   height=13, width=len(pricename) + 20,\
                   seconds=config_settings[6],\
                   ok_label='Muuda', cancel_label='Välju')

    if code == d.OK:
        pass
    else:
        exit()

# alammenüü moodulite käivitamine
def tui_main():
    d = Dialog(dialog='dialog')
    d.set_background_title('E-Balansave energiasäästur')

    while not test_connection():
        tui_connection_check(d)

    config_mode = tui_settings(d)

    if config_mode == '1':
        pass
    elif config_mode == '2':
        config_write([tui_select_prices(d),\
                           tui_select_pricelimit_critical(d),\
                           tui_select_pricelimit(d),\
                           tui_select_temperaturelimit(d),\
                           tui_select_checkintervallimit(d)])
    else:
        exit()

    config_settings = config_read()

    while True:
        if test_connection():
            current_price, temperature, pin_switched = main(config_settings)
            tui_display_progress(d, config_settings, current_price, temperature, pin_switched)
        else:
            tui_connection_check(d)

# programmi käivitamine
clear_screen()
tui_main()
